import React from "react";
import Header from "./components/header";
import Footer from "./components/footer";
import GlossaryItems from "./components/glossaryitems";
import MainContextProvider from "./contexts/maincontext";
import "./App.css";
function App() {
  return (
    <MainContextProvider>
      <Header />
      <main className="p-lg-4 pt-3">
        <GlossaryItems />
      </main>
      <Footer />
    </MainContextProvider>
  );
}

export default App;
