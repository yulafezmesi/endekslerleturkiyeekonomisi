import React, { createContext, useState, useEffect } from "react";
export const MainContext = createContext();
const MainContextProvider = (props) => {
  const getJsonArrayFromData = (data) => {
    var obj = {};
    var result = [];
    var headers = data[0];
    var cols = headers.length;
    var row = [];
    for (var i = 1, l = data.length; i < l; i++) {
      // get a row to fill the object
      row = data[i];
      // clear object
      obj = {};
      for (var col = 0; col < cols; col++) {
        // fill object with new values
        if (!row[col]) {
          row[col] = "";
        } else {
          obj[headers[col]] = row[col];
        }
      }
      // add object in a final result
      result.push(obj);
    }
    return result;
  };

  const [glossary, setGlossary] = useState([]);
  const [pageContent, setPageContent] = useState([]);
  const [isGlossaryLoading, setIsGlossaryLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        let data = await fetch(
          "https://sheets.googleapis.com/v4/spreadsheets/1vzi-RR5H6vNAlGF8Ss6eM65WNZnw8IZKnBsl6xgwmEY/values/icerik!A:E?majorDimension=ROWS&key=AIzaSyDC_sEIhdm6_okUh3n2pwNXEVy3UTp1D4E"
        );
        let { values } = await data.json();
        if (values.length) {
          setPageContent(getJsonArrayFromData(values));
        }
      } catch (e) {
        alert(e);
      }
    };
    fetchData();
  }, []);
  const getFilteredContent = (sectionId, order, type, lang) => {
    if (pageContent.length) {
      const filtered = pageContent.find(
        (item) =>
          item.lang === lang &&
          item.section_id === sectionId &&
          item.order === order &&
          item.type === type
      );
      return filtered.content;
    }
  };
  const getGlossary = async () => {
    try {
      let data = await fetch(
        "https://sheets.googleapis.com/v4/spreadsheets/1vzi-RR5H6vNAlGF8Ss6eM65WNZnw8IZKnBsl6xgwmEY/values/Kitap!B:Q?majorDimension=ROWS&key=AIzaSyDC_sEIhdm6_okUh3n2pwNXEVy3UTp1D4E"
      );
      let { values } = await data.json();
      if (values.length) {
        setGlossary(getJsonArrayFromData(values));
        setIsGlossaryLoading(false);
        return getJsonArrayFromData(values);
      }
    } catch (e) {
      alert(e);
    }
  };

  return (
    <MainContext.Provider
      value={{
        getGlossary,
        glossary,
        isGlossaryLoading,
        getFilteredContent,
      }}
    >
      {props.children}
    </MainContext.Provider>
  );
};
export default MainContextProvider;
