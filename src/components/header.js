import React, { useState  } from "react"
import PropTypes from "prop-types"
import Socials from "./socials"
import HamburgerMenu from "react-hamburger-menu"
import Logo from "../images/logo.png"
const Header = ({ siteTitle }) => {
  const [open, setOpen] = useState(false)
  return (
    <div>
      <nav className="scroll-down">
        <div className="nav">
          <div className="nav-container">
            <div className="nav-container-logo">
              <a href={`/`}>
                <img alt="Verimetrik Logo" src={Logo} />
              </a>
            </div>
            <div className="nav-container-hamburger">
              <HamburgerMenu
                isOpen={open}
                menuClicked={() => {
                  setOpen(!open)
                }}
                width={26}
                height={26}
                strokeWidth={2}
                rotate={0}
                color="#113377"
                borderRadius={0}
                transition="none"
              ></HamburgerMenu>
              <div
                className={
                  open
                    ? "nav-container-hamburger-open"
                    : `nav-container-hamburger-open nav-container-hamburger-close`
                }
              >
                <ul>
                  <div className="nav-container-menu-social">
                    <Socials />
                  </div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
