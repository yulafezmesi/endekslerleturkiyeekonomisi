import React, { useState, useEffect, useRef, useContext } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { MainContext } from "../contexts/maincontext";
import "../style/main.scss";
import HomeImg from "../images/endekslerle-turkiye-ekonomisi-rgb.jpg";
import Glossary from "./glossary";
const GlossaryItems = () => {
  const { getGlossary, isGlossaryLoading } = useContext(MainContext);
  const [glossary, setGlossary] = useState([]);
  const [letter, setLetter] = useState([]);
  const glossaryContainerRef = useRef(null);
  const scrollTo = (id) => {
    let ref = document.getElementById(id);
    if (ref /* + other conditions */) {
      var topPos = ref.offsetTop;
      glossaryContainerRef.current.scrollTop = topPos - 10;
    }
  };
  const checkElemisOnTop = () => {
    var sections = {};
    var section = document.querySelectorAll(".group-class div");
    Array.prototype.forEach.call(section, function (e) {
      sections[e.id] = e.offsetTop;
    });
    var scrollPosition = glossaryContainerRef.current.scrollTop;

    for (let [key, value] of Object.entries(sections)) {
      if (value <= scrollPosition + 10) {
        let activeClass = document.querySelector(".active");
        if (activeClass) {
          activeClass.setAttribute("class", " ");
        }
        document
          .querySelector(".glossary-letters a[href*=" + "letter-" + key + "]")
          .setAttribute("class", "active");
      }
    }
  };

  useEffect(() => {
    getGlossary().then((data) => {
      let glossaryItems = data
        .sort((a, b) => a.URLID.localeCompare(b.URLID))
        .reduce((r, e) => {
          // get first letter of name of current element
          let group = e.URLID[0];
          // if there is no property in accumulator with this letter create it
          if (!r[group]) r[group] = { group, children: [e] };
          // if there is push current element to children array for that letter
          else r[group].children.push(e);
          // return accumulator
          return r;
        }, []);
      let result = Object.values(glossaryItems);
      setGlossary(result);
      setLetter(result.map((item) => item.group));
    });
  }, []);
  return (
    <Router>
      <div className="mt-7 mb-4 grid ">
        <div className="glossary">
          <div className="glossary-items grid ">
            <div className="d-flex  border-shadow stick ">
              <div className="glossary-letters  ">
                <ul>
                  {letter.map((item, i) => (
                    <li key={item} onClick={() => scrollTo(item)}>
                      <a
                        className={`glossary-letters-link-${item}`}
                        href={`#letter-${item}`}
                      >
                        {item}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
              <div className="glossary-titles">
                <ul
                  onScroll={() => checkElemisOnTop()}
                  ref={glossaryContainerRef}
                >
                  {glossary.map((item, index) => (
                    <div key={index} className="group-class">
                      <div id={item.group}>
                        <li
                          className="glossary-titles-group pb-1 pt-1"
                          key={item.group}
                        >
                          {item.group}
                        </li>
                      </div>

                      {item.children.map((item2, i) => (
                        <li key={i}>
                          <Link
                            activeClassName="glossary-titles-active"
                            to={`/${item2.URLID}`}
                          >
                            {item2.Başlık}
                          </Link>
                        </li>
                      ))}
                    </div>
                  ))}
                </ul>
                <span className="visible-bottom" id="visibleBottomRef"></span>
              </div>
            </div>
            <div className="glossary-content p-3 mt-1-sm border-shadow">
              {!isGlossaryLoading ? (
                <Switch>
                  <Route component={Glossary} path="/:id"></Route>
                  <Route path="/">
                    <div
                      style={{
                        display: "flex",
                        width: "100%",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <a
                        style={{
                          height: "70vh",
                        }}
                        target="_blank"
                        href="http://www.remzi.com.tr/kitap/endekslerle-turkiye-ekonomisi"
                      >
                        <img
                          alt="Endekslerle Türkiye Ekonomisi Remzi Kitap Evi"
                          style={{
                            objectFit: "contain",
                            maxHeight: "100%",
                            alignSelf: "flex-start",
                          }}
                          src={HomeImg}
                        ></img>
                      </a>
                    </div>
                  </Route>
                </Switch>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </Router>
  );
};

export default GlossaryItems;
