import React, { useEffect, useState, useContext } from "react";
import { Helmet } from "react-helmet";
import "../style/main.scss";
import { MainContext } from "../contexts/maincontext";
const GlossaryItems = ({ match }) => {
  let id = match.params.id;
  const { glossary } = useContext(MainContext);
  const [glossaryDetail, setGlossaryDetail] = useState({});
  useEffect(() => {
    const detail = glossary.find((item) => item.URLID === id);
    try {
      let titleIdArray = detail.İV.split(",");
      let titles = [];
      titleIdArray.forEach((item) => {
        if (detail.ID !== item) {
          let title = glossary.find((item2) => item2.ID === item);
          if (title !== undefined) {
            titles.push(title);
          }
        }
      });
    } catch (e) {}
    setGlossaryDetail(detail);
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      let visibleBottomRef = document.getElementById("visibleBottomRef");
      setTimeout(() => {
        visibleBottomRef.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "start",
        });
      }, 800);
    }
  }, [id]);
  const htmlToElement = (html) => {
    var template = document.createElement("template");
    if (html) {
      html = html.trim(); // Never return a text node of whitespace as the result
      template.innerHTML = html;
      return template.content.firstChild.src.replace("interactive", "image");
    } else {
      return null;
    }
  };
  return glossaryDetail ? (
    <>
      <Helmet>
        <title>{glossaryDetail.Başlık}</title>
        <meta name="description" content={glossaryDetail.Başlık} />
        <meta name="theme-color" content="#00000" />
      </Helmet>
      <div className="glossary-content-container">
        <h2>{glossaryDetail.Başlık}</h2>
      </div>
      {glossaryDetail.CHURL ? (
        <>
          <h4 className="mb-md-0">Grafik</h4>
          <div className="chart-wrapper-is-mobile">
            <img
              alt={glossaryDetail.Başlık}
              src={htmlToElement(glossaryDetail.CHURL)}
            />
          </div>
          <div
            className="chart-wrapper"
            dangerouslySetInnerHTML={{ __html: glossaryDetail.CHURL }}
          ></div>
        </>
      ) : null}
    </>
  ) : null;
};

export default GlossaryItems;
