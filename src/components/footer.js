import React, { useContext } from "react";
import { MainContext } from "../contexts/maincontext";
function Section() {
  const { getFilteredContent } = useContext(MainContext);
  return (
    <footer id="footer" className="footer">
      <div className="grid justify-content-center flex-wrap d-flex grid-pad pl-lg-5 pr-lg-5">
        <div
          dangerouslySetInnerHTML={{
            __html: getFilteredContent("footer-glossary", "1", "Text", "tr"),
          }}
        ></div>
      </div>
    </footer>
  );
}

export default Section;
